Rails.application.routes.draw do
  devise_for :users
  root to: 'trombones#index'
  resources :trombones
  get 'users/:screen_name', to: 'users#show', as: :user, constraints: { :screen_name => %r{[^\/]+} }
end

