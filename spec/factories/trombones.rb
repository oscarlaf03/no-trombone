FactoryBot.define do
  factory :trombone do
    status { Faker::StarWars.quote.truncate(140) }
    photo { nil }
    user
  end
end
