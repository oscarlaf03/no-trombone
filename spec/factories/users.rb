
FactoryBot.define do
  factory :user do
    name { Faker::Name.first_name.split(' ').first.truncate(13) }
    screen_name { Faker::DragonBall.character.split(' ').join.truncate(12) }
    email {Faker::Internet.safe_email }
    password { '123123'}
    password_confirmation '123123'
    photo { nil }
  end
end
