require 'rails_helper'

RSpec.describe UserPolicy do
  subject { described_class.new(user, user) }

  let(:user) { create(:user) }

  context 'Altering others data' do
    let(:user) { create(:user) }
    it { is_expected.to forbid_actions([:update, :edit]) }
  end

end
