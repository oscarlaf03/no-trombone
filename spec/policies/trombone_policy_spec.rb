require 'rails_helper'

RSpec.describe TrombonePolicy do

  subject { described_class.new(user, trombone) }
  let(:trombone) { create(:trombone)}
  let(:user) { trombone.user }

  context 'Altering my own trombone' do
    it { is_expected.to permit_actions([:update, :destroy, :edit]) }
  end

  context 'Altering another ones trombone' do
    let (:user) { create(:user) }
    it { is_expected.to forbid_actions([:update, :destroy, :edit]) }
  end

  context 'Unsigned user viewing trombones' do
    let (:user) { nil }
    it { is_expected.to permit_actions([:index, :show])}
  end

  context 'Signed user creates trombone' do
    let (:user) { create(:user) }
    it { is_expected.to permit_actions([:new, :create]) }
  end
end
