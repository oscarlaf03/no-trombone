require 'rails_helper'

RSpec.describe TrombonesController, type: :controller do
  routes { Rails.application.routes }

  let(:t) { create(:trombone)}
  let(:user) { t.user}

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      sign_in user

      get :show, params: {id: t.id }
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #new" do
    it "returns http success" do
      sign_in user

      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #edit" do
    it "returns http success" do
      sign_in user
      puts user.inspect
      get :edit, params: {id: t.id }
      expect(response).to have_http_status(:success)
    end
  end

end
