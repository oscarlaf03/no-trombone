require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  routes { Rails.application.routes }

  let(:t) { create(:trombone)}
  let(:user) { t.user}

  describe "GET #show" do
    it "returns http success" do
      sign_in user
      get :show, params: {screen_name: user.screen_name}
      expect(response).to have_http_status(:success)
    end
  end

end
