require 'rails_helper'

RSpec.describe User, type: :model do
  context 'Invalid when ' do
    it 'screen_name is already taken' do
      u = create(:user)
      u2 = build(:user, screen_name: u.screen_name)
      expect(u2.valid?).to be(false)
    end
    it 'email is not a real email' do
      u = build(:user, email: 'user@user')
      expect(u.valid?).to be(false)
    end
  end
end
