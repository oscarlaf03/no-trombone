require 'rails_helper'
RSpec.describe Trombone, type: :model do
  context 'Ivalid when' do
    it 'status is over 140 characters'  do
      t = build(:trombone, status: Faker::ChuckNorris.fact * 10 )
      expect(t.valid?).to be(false)
    end
  end
  context 'Valid when' do
    it 'status is less 140 characters'  do
      t = build(:trombone, status: Faker::ChuckNorris.fact.truncate(140))
      expect(t.valid?).to be(true)
    end
  end
end

