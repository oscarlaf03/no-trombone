# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# User.destroy_all
# Trombone.destroy_all

screen_names = []

20.times do
  name = Faker::Fallout.character
  screen_names << name  unless screen_names.include?(name)
end

20.times do
  name = Faker::Pokemon.name
  screen_names << name  unless screen_names.include?(name)
end

20.times do
  name = Faker::StarWars.character
  screen_names << name  unless screen_names.include?(name)
end

20.times do
  name = Faker::Hobbit.character
  screen_names << name  unless screen_names.include?(name)
end

20.times do
  name = Faker::HarryPotter.character
  screen_names << name  unless screen_names.include?(name)
end


20.times do
  name = Faker::Ancient.god
  screen_names << name  unless screen_names.include?(name)
end

20.times do
  name = Faker::Ancient.primordial
  screen_names << name  unless screen_names.include?(name)
end


20.times do
  name = Faker::Ancient.titan
  screen_names << name  unless screen_names.include?(name)
end


20.times do
  name = Faker::Ancient.hero
  screen_names << name  unless screen_names.include?(name)
end

quotes = []

20.times do
  text =  Faker::Simpsons.quote
  quotes << text unless quotes.include?(text)
end

20.times do
  text =  Faker::StarWars.quote
  quotes << text unless quotes.include?(text)
end

20.times do
  text = Faker::Hobbit.quote
  quotes << text unless quotes.include?(text)
end

20.times do
  text = Faker::HarryPotter.quote
  quotes << text  unless quotes.include?(text)
end

20.times do
  text = Faker::ChuckNorris.fact
  quotes << text unless quotes.include?(text)
end

20.times do
  text = Faker::DrWho.quote,
  quotes << text unless quotes.include?(text)
end


User.all.select { |u| u.trombones.empty? }.each do |user|
  rand(3..6).times do
    t = user.trombones.create(
      user: user,
      status: quotes.uniq.sample.truncate(129)
    )
  puts "by: #{t.user.screen_name}\n#{t.status}\n\n\n" if t.save
  end
end


# puts "*************\n\n\n\n\n\n**********"
# puts "*************Creating #{screen_names.uniq.size}  users**********"
# puts "*************\n\n\n\n\n\n**********"

# screen_names.uniq.each do |screen_name|
#   user = User.new(name: Faker::Name.first_name.split(' ').first.truncate(13),
#                   password: '123123',
#                   screen_name: screen_name.split(' ').first.truncate(12),
#                   # remote_photo_url: Faker::Avatar.image,
#                   remote_photo_url:  UiFaces.face,
#                   test: true
#                   )
#   user.email =  Faker::Internet.safe_email(user.name)
#   user.save
#   puts "created user: #{user.name}\nemail: #{user.email}\nscreen name: #{user.screen_name}\n\n\n" if user.save
# end

# rand(40..60).times do
#   user = User.all.sample
#   t = Trombone.create(
#     user: user,
#     status: quotes.uniq.sample.truncate(129)
#     )
#   puts "by: #{t.user.screen_name}\n#{t.status}\n\n\n" if t.save
# end

# oscar = User.find_by(email: 'ortizg.oscar@gmail.com')

# if oscar
#   10.times do
#     user = oscar
#     t = Trombone.create(
#       user: user,
#       status: quotes.uniq.sample.truncate(129)
#       )
#     puts "by: #{t.user.screen_name}\n#{t.status}\n\n\n" if t.save
#   end
# end




puts User.count
puts Trombone.count
