class CreateTrombones < ActiveRecord::Migration[5.1]
  def change
    create_table :trombones do |t|
      t.text :status
      t.string :photo
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
