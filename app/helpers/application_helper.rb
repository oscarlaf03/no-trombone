module ApplicationHelper

  def user_photo(user)
    if user.photo.present?
      (cl_image_path user.photo)
    else
      'https://res.cloudinary.com/dkkvtcelc/image/upload/q_auto:eco/v1537372686/home_page/User-placeholder.png'
    end
  end

  def owner?(trombone)
    user_signed_in? ? trombone.user == current_user : false
  end

  def time(input)
    # input.kind_of?(Time) ? input.getlocal.strftime("%I:%M %p") : input.to_time.getlocal.strftime("%I:%M %p")
    t = input.kind_of?(Time) ? input : input.to_time
    from = Time.now - (Time.now - t)
    time_ago_in_words(from)
  end

  def long_date(date_object)
    I18n.l date_object, format: :long
  end

  def short_date(date_object)
    I18n.l date_object, format: :short
  end

  def search?
    params[:query].present?
  end
end
