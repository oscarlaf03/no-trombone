class UsersController < ApplicationController
  skip_before_action :authenticate_user!, only: [:show]

  def show
    @user = User.find_by_screen_name(params[:screen_name])
    authorize @user
    @trombones = @user.trombones.order(created_at: :desc)
  end
end
