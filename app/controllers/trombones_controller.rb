class TrombonesController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]
  before_action :set_trombone, only: [:edit, :show, :destroy, :edit, :update]

  def index
    skip_authorization
    skip_policy_scope
    @trombone = Trombone.new
    if params[:query].present?
      sql_query = " \
        trombones.status @@ :query \
        OR users.name @@ :query \
        OR users.screen_name @@ :query \
      "
      @trombones = Trombone.joins(:user).where(sql_query, query: "%#{params[:query]}%").order(created_at: :desc)
    else
      @trombones = Trombone.all.order(created_at: :desc)
    end
  end

  def show
  end

  def new
    @trombone = Trombone.new
    authorize @trombone
  end

  def create
    @trombone = current_user.trombones.create(set_params)
    authorize @trombone
    if @trombone.save
      respond_to do |format|
        format.html {redirect_to trombones_path}
        format.js
      end
    else
      respond_to do |format|
        format.html { }
        format.js
      end
    end
  end

  def edit
  end

  def update
    @trombone.update(set_params)
    authorize @trombone
    if @trombone.save
      respond_to do |format|
        format.html {redirect_to trombones_path}
      end
    end
  end

  def destroy
    @trombone.destroy
    redirect_to trombones_path
  end

  private

  def set_trombone
    @trombone = Trombone.find(params[:id])
    authorize @trombone
  end

  def set_params
    params.require(:trombone).permit(:status, :photo, :photo_cache)
  end
end
