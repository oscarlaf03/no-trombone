class Trombone < ApplicationRecord
  belongs_to :user
  validates :status, presence: true
  validates :status, length: { maximum: 140 }
  mount_uploader :photo, PhotoUploader
end
