class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_many :trombones, dependent: :destroy
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :email, format: { with: VALID_EMAIL_REGEX }
  VALID_SCREEN_NAME = %r{[^\/]+}
  validates :screen_name, format: { with: VALID_SCREEN_NAME }
  validates :name, :screen_name, :email, presence: true
  validates :screen_name, length: { maximum: 13 }
  validates :screen_name, uniqueness: true
  mount_uploader :photo, PhotoUploader

  def to_param
    screen_name
  end
end
